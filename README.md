## BVA table

| Parameter        | Equivalence class           |
|------------------|-----------------------------|
| distance         |`<=0`,`>0`                  |
| type             |`budget`,`luxury`,`nonsense`      |
| plan             |`minute`,`fixed_price`, `nonsense`|
| planned_distance |`<=0`,`>0`                  |
| time             |`<=0`,`>0`                  |
| planned_time     |`<=0`,`>0`                  |
| inno_discount    |`yes`,`no`,`nonsense`       |

## Parameters
Budet car price per minute = 24
Luxury car price per minute = 53
Fixed price per km = 14
Allowed deviations in % = 15
Inno discount in % = 12

## Issues

- Discount is not calculated at all, neither with budget nor with lulury cars, neither with minute nor with fixed plan. 
- Calculations with budget cars are wrong in general both on fixed and on minute plan


## Test Cases

| Type | Plan | Distance | Planned distance | Time | Planned Time | Discount | Actual | Expected | Pass/Fail |
|--------|--------|---|---|---|---|-----|---|---|---|
| budget | minute | 10 | 10 | 10 | 10 | yes | 287.04 | 211.2 | failed |
| budget | minute | 10 | 10 | 10 | 10 | no | 312 | 240 | failed |
| budget | minute | 10 | 10 | 20 | 10 | yes | 574.08 | 422.4 | failed |
| budget | minute | 10 | 10 | 20 | 10 | no | 624 | 480 | failed |
| budget | minute | 10 | 20 | 10 | 10 | yes | 287.04 | 211.2 | failed |
| budget | minute | 10 | 20 | 10 | 10 | no | 312 | 240 | failed |
| budget | minute | 10 | 20 | 20 | 10 | yes | 574.08 | 422.4 | failed |
| budget | minute | 10 | 20 | 20 | 10 | no | 624 | 480 | failed |
| budget | minute | 20 | 10 | 10 | 10 | yes | 287.04 | 211.2 | failed |
| budget | minute | 20 | 10 | 10 | 10 | no | 312 | 240 | failed |
| budget | minute | 20 | 10 | 20 | 10 | yes | 574.08 | 422.4 | failed |
| budget | minute | 20 | 10 | 20 | 10 | no | 624 | 480 | failed |
| budget | minute | 20 | 20 | 10 | 10 | yes | 287.04 | 211.2 | failed |
| budget | minute | 20 | 20 | 10 | 10 | no | 312 | 240 | failed |
| budget | minute | 20 | 20 | 20 | 10 | yes | 574.08 | 422.4 | failed |
| budget | minute | 20 | 20 | 20 | 10 | no | 624 | 480 | failed |
| budget | fixed_price | 10 | 10 | 10 | 10 | yes | 115 | 123.2 | failed |
| budget | fixed_price | 10 | 10 | 10 | 10 | no | 125 | 140 | failed |
| budget | fixed_price | 10 | 10 | 20 | 10 | yes | 306.6666666666667 | 123.2 | failed |
| budget | fixed_price | 10 | 10 | 20 | 10 | no | 333.3333333333333 | 140 | failed |
| budget | fixed_price | 10 | 20 | 10 | 10 | yes | 153.33333333333334 | 123.2 | failed |
| budget | fixed_price | 10 | 20 | 10 | 10 | no | 166.66666666666666 | 140 | failed |
| budget | fixed_price | 10 | 20 | 20 | 10 | yes | 306.6666666666667 | 123.2 | failed |
| budget | fixed_price | 10 | 20 | 20 | 10 | no | 333.3333333333333 | 140 | failed |
| budget | fixed_price | 20 | 10 | 10 | 10 | yes | 153.33333333333334 | 211.2 | failed |
| budget | fixed_price | 20 | 10 | 10 | 10 | no | 166.66666666666666 | 240 | failed |
| budget | fixed_price | 20 | 10 | 20 | 10 | yes | 306.6666666666667 | 422.4 | failed |
| budget | fixed_price | 20 | 10 | 20 | 10 | no | 333.3333333333333 | 480 | failed |
| budget | fixed_price | 20 | 20 | 10 | 10 | yes | 230 | 246.4 | failed |
| budget | fixed_price | 20 | 20 | 10 | 10 | no | 250 | 280 | failed |
| budget | fixed_price | 20 | 20 | 20 | 10 | yes | 306.6666666666667 | 246.4 | failed |
| budget | fixed_price | 20 | 20 | 20 | 10 | no | 333.3333333333333 | 280 | failed |
| luxury | minute | 10 | 10 | 10 | 10 | yes | 487.6 | 466.4 | failed |
| luxury | minute | 10 | 10 | 10 | 10 | no | 530 | 530 | passed |
| luxury | minute | 10 | 10 | 20 | 10 | yes | 975.2 | 932.8 | failed |
| luxury | minute | 10 | 10 | 20 | 10 | no | 1060 | 1060 | passed |
| luxury | minute | 10 | 20 | 10 | 10 | yes | 487.6 | 466.4 | failed |
| luxury | minute | 10 | 20 | 10 | 10 | no | 530 | 530 | passed |
| luxury | minute | 10 | 20 | 20 | 10 | yes | 975.2 | 932.8 | failed |
| luxury | minute | 10 | 20 | 20 | 10 | no | 1060 | 1060 | passed |
| luxury | minute | 20 | 10 | 10 | 10 | yes | 487.6 | 466.4 | failed |
| luxury | minute | 20 | 10 | 10 | 10 | no | 530 | 530 | passed |
| luxury | minute | 20 | 10 | 20 | 10 | yes | 975.2 | 932.8 | failed |
| luxury | minute | 20 | 10 | 20 | 10 | no | 1060 | 1060 | passed |
| luxury | minute | 20 | 20 | 10 | 10 | yes | 487.6 | 466.4 | failed |
| luxury | minute | 20 | 20 | 10 | 10 | no | 530 | 530 | passed |
| luxury | minute | 20 | 20 | 20 | 10 | yes | 975.2 | 932.8 | failed |
| luxury | minute | 20 | 20 | 20 | 10 | no | 1060 | 1060 | passed |
| luxury | fixed_price | 10 | 10 | 10 | 10 | yes | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 10 | 10 | 10 | 10 | no | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 10 | 10 | 20 | 10 | yes | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 10 | 10 | 20 | 10 | no | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 10 | 20 | 10 | 10 | yes | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 10 | 20 | 10 | 10 | no | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 10 | 20 | 20 | 10 | yes | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 10 | 20 | 20 | 10 | no | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 20 | 10 | 10 | 10 | yes | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 20 | 10 | 10 | 10 | no | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 20 | 10 | 20 | 10 | yes | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 20 | 10 | 20 | 10 | no | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 20 | 20 | 10 | 10 | yes | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 20 | 20 | 10 | 10 | no | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 20 | 20 | 20 | 10 | yes | Invalid Request | Invalid Request | passed |
| luxury | fixed_price | 20 | 20 | 20 | 10 | no | Invalid Request | Invalid Request | passed |
| nonsense | minute | 10 | 10 | 10 | 10 | yes | Invalid Request | Invalid Request | passed
| budget | nonsense | 10 | 10 | 10 | 10 | yes | Invalid Request | Invalid Request | passed
| budget | minute | -1 | 10 | 10 | 10 | yes | Invalid Request | Invalid Request | passed
| budget | minute | 10 | -1 | 10 | 10 | yes | Invalid Request | Invalid Request | passed
| budget | minute | 10 | 10 | -1 | 10 | yes | Invalid Request | Invalid Request | passed
| budget | minute | 10 | 10 | 10 | -1 | yes | Invalid Request | Invalid Request | passed
| budget | minute | 10 | 10 | 10 | 10 | nonsense | Invalid Request | Invalid Request | passed
